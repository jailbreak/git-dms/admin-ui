function isNonEmptyString(v: unknown): boolean {
	if (v === null || v === undefined) {
		return false
	}
	if (typeof v === "string") {
		return v.length > 0
	}
	return false
}

export interface Config {
	gitlabBotAccessToken: string
	gitlabBotUserName: string
	gitlabOauthClientId: string
	gitlabUrl: string
}

export function loadConfigFromEnv(): Config {
	const gitlabBotAccessToken = import.meta.env.VITE_GITLAB_BOT_ACCESS_TOKEN
	if (!isNonEmptyString(gitlabBotAccessToken)) {
		throw new Error(`VITE_GITLAB_BOT_ACCESS_TOKEN environment variable must be a non-empty string`)
	}

	const gitlabBotUserName = import.meta.env.VITE_GITLAB_BOT_USER_NAME as string
	if (!isNonEmptyString(gitlabBotUserName)) {
		throw new Error(`VITE_GITLAB_BOT_USER_NAME environment variable must be a non-empty string`)
	}

	const gitlabOauthClientId = import.meta.env.VITE_GITLAB_OAUTH_CLIENT_ID
	if (!isNonEmptyString(gitlabOauthClientId)) {
		throw new Error(`VITE_GITLAB_OAUTH_CLIENT_ID environment variable must be a non-empty string`)
	}

	const gitlabUrl = import.meta.env.VITE_GITLAB_URL || "https://gitlab.com"

	return {
		gitlabBotAccessToken,
		gitlabBotUserName,
		gitlabOauthClientId,
		gitlabUrl,
	}
}
