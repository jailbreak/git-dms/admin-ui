import type { RequestHandler } from "@sveltejs/kit"
import { Gitlab } from "@gitbeaker/node"
import { buildActions, fetchProject, getProject } from "$lib/infrastructure/gitlab"
import type { Project } from "$lib/storage"

export const post: RequestHandler = async ({ locals, body }) => {
	const { gitlabBotAccessToken } = locals.config

	const projectName = body["projectName"]
	const filteredFiles = body["filteredFiles"]
	const previousFiles = body["previousFiles"]
	const removedFiles = body["removedFiles"]
	let contributionComment = body["contributionComment"]
	const files = body["files"]
	const email = body["email"]

	const gitlabClient = new Gitlab({
		host: locals.config.gitlabUrl,
		oauthToken: gitlabBotAccessToken,
	})

	if (contributionComment === "" || files.length === 0 || email === "") {
		return {
			body: {
				error: "Can not sent a contribution without a comment, a resources update and a email",
			},
		}
	}

	const { Branches, Commits, MergeRequests, Projects, RepositoryFiles } = gitlabClient

	const project: Project | null = await fetchProject(gitlabClient, projectName)

	if (project === null) {
		return {
			body: {
				error: "Can not find project to contribute to",
			},
		}
	}

	const dataPackageRaw = await RepositoryFiles.showRaw(project.id, "datapackage.json", "HEAD")
	const dataPackageJson = JSON.parse(dataPackageRaw.toString())

	dataPackageJson.resources = []

	let contributorProject: Project | null = null
	let fork
	try {
		// try to create a fork
		fork = await Projects.fork(project.id)
	} catch {
		// if bot has already forked the project, retrieve it
		const forks = await Projects.forks(project.id)
		// force pull
		fork = forks.find((f) => f.owner.username === locals.config.gitlabBotUserName)
	}

	if (fork) {
		contributorProject = getProject(fork)
	} else {
		return {
			body: {
				error: "Can not fork project to contribute",
			},
		}
	}

	// Add the contributor in datapackage.json if is not already present
	const contributorEntry = {
		title: email,
		role: "contributor",
	}
	if (dataPackageJson.contributors) {
		const contributors = dataPackageJson.contributors.map((contributor) => contributor.title)
		if (!contributors.includes(email)) {
			dataPackageJson.contributors.push(contributorEntry)
		}
	} else {
		dataPackageJson.contributors = [contributorEntry]
	}

	// create a branch with the timestamp and the project name
	const branchName = `${Date.now()}-${contributorProject.name}`
	try {
		await Branches.create(contributorProject.id, branchName, project.defaultBranch)
	} catch {
		return {
			body: {
				error: `Can not create contribution branch ${branchName}`,
			},
		}
	}

	console.log(`${branchName} created on ${contributorProject.name} : ${contributorProject.url}/-/commits/${branchName}`)

	const actions = await buildActions(
		gitlabClient,
		project,
		dataPackageJson,
		files,
		filteredFiles,
		previousFiles,
		removedFiles
	)

	// prefix contributionComment with [email]
	contributionComment = `[${email}] ${contributionComment}`

	// Commit updated datapackage.json and resources
	await Commits.create(contributorProject.id, branchName, contributionComment, actions)

	// Create a merge request
	const mergeRequest = await MergeRequests.create(
		contributorProject.id,
		branchName,
		project.defaultBranch,
		contributionComment,
		{
			targetProjectId: project.id,
			remove_source_branch: true,
		}
	)

	const mergeRequestUrl = mergeRequest.web_url

	return {
		body: {
			mergeRequestUrl: `${mergeRequestUrl}`,
		},
	}
}
