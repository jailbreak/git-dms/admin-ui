import type { RequestHandler } from "@sveltejs/kit"
import { Gitlab } from "@gitbeaker/node"
import { fetchDataPackage, fetchProject } from "$lib/infrastructure/gitlab"
import type { DataPackage } from "$lib/storage"

export const get: RequestHandler = async (request) => {
	const { query, locals } = request
	const project = query.get("project")

	const { gitlabBotAccessToken } = locals.config
	const gitlabClient = new Gitlab({
		host: locals.config.gitlabUrl,
		oauthToken: gitlabBotAccessToken,
	})
	const dataPackage: DataPackage = await fetchDataPackage(gitlabClient, project)
	const selectedProject = await fetchProject(gitlabClient, project)

	return {
		body: {
			dataPackage: dataPackage,
			selectedProject: selectedProject,
		},
	}
}
