import BaseError from "baseerr"

export class CreateProjectError extends BaseError<string> {}
export class CreateProjectDuplicateError extends CreateProjectError {}
