import queryString from "query-string"
import { setContext } from "svelte"
import { writable } from "svelte/store"
import { browser } from "$app/env"
import { goto } from "$app/navigation"
import * as storage from "$lib/infrastructure/storage"
import { AUTH_CONTEXT_KEY } from "$lib/constants"

export function useAuth({ authRoutes, onTokensLoad, page, pkceConfig }) {
	function gotoLoginIfAnonymous(page) {
		if (browser && tokensFromStorage === null) {
			goto(buildLoginPath(page))
		}
	}

	function buildLoginPath(page) {
		return queryString.stringifyUrl({
			url: authRoutes.login,
			query: { returnTo: buildReturnTo(page) },
		})
	}

	function buildLogoutPath(page) {
		return queryString.stringifyUrl({
			url: authRoutes.logout,
			query: { returnTo: buildReturnTo(page) },
		})
	}

	function buildReturnTo(page) {
		return queryString.stringifyUrl({
			// Avoid auth routes being returnTo URLs.
			url: Object.values(authRoutes).includes(page.path)
				? "/" // TODO Handle base path
				: page.path,
			query: Object.fromEntries(page.query.entries()),
		})
	}

	let tokensFromStorage = undefined
	const tokens = writable(undefined)

	setContext(AUTH_CONTEXT_KEY, {
		buildLoginPath,
		buildLogoutPath,
		gotoLoginIfAnonymous,
		pkceConfig,
		tokens,
	})

	if (browser) {
		// If we are already on a login, callback or logout page, do not authenticate.
		if (Object.values(authRoutes).includes(page.path)) {
			return
		}
		tokensFromStorage = storage.getTokens()
		tokens.set(tokensFromStorage)
	}
	onTokensLoad(tokensFromStorage)

	return { buildLoginPath, buildLogoutPath, gotoLoginIfAnonymous, tokens }
}
