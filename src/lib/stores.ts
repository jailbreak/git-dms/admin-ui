import { writable } from "svelte/store"
import type { Writable } from "svelte/store"
import type { User } from "./model"

export const userProfile: Writable<User | null | undefined> = writable(undefined)
