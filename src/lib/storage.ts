export interface Project {
	id: number
	name: string
	defaultBranch: string
	url: string
	ownerName: string
}

export interface ProjectFilter {
	onlyDataPackages?: boolean
	onlyNoDataPackages?: boolean
	reload?: boolean
}

export interface Ressource {
	path: string
	url: string
}

export interface ReadenFile {
	name: string
	content: string
}

export interface DataPackage {
	name: string
	description: string
	resources: Array<Ressource>
	projectUrl: string
	minisiteUrl: string
}

export interface Action {
	action: string
	file_path: string
	content?: string
}

export interface StorageAdapter {
	createProject(name: string): Promise<Project>
	createDataPackage(project: Project, formData: FormData, filteredFiles: Array<string>): Promise<boolean>
	editDataPackage(
		project: Project,
		formData: FormData,
		filteredFiles: Array<string>,
		previousFiles: Array<string>,
		removedFiles: Array<string>
	)
	contributeToDataPackage(
		project: Project,
		formData: FormData,
		filteredFiles: Array<string>,
		previousFiles: Array<string>,
		removedFiles: Array<string>
	)
	fetchDataPackage(projectUrl: string): Promise<DataPackage | null>
	fetchProject(projectUrl: string): Promise<Project | null>
	fetchProjects(filter?: ProjectFilter): Promise<Array<Project>>
}
