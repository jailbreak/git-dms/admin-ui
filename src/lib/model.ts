export interface User {
	avatarUrl: string
	displayName: string
	profileUrl: string
	username: string
}
