import type { ReadenFile } from "$lib/storage"

export function readUploadedFileAsText(inputFile: File): Promise<string> {
	const reader = new FileReader()

	return new Promise((resolve, reject) => {
		reader.onerror = () => {
			reader.abort()
			reject(new DOMException("Problem parsing input file."))
		}

		reader.onload = () => {
			resolve(reader.result as string)
		}
		reader.readAsText(inputFile)
	})
}

export const filesToReadenFiles = async (formDataFiles: Array<File>): Promise<Array<ReadenFile>> => {
	const files = Promise.all(
		formDataFiles.map(async (file) => {
			if (!file) {
				return {
					name: "",
					content: "",
				} as ReadenFile
			} else {
				const content = await readUploadedFileAsText(file)
				return {
					name: file.name,
					content: content,
				} as ReadenFile
			}
		})
	)
	return files
}
