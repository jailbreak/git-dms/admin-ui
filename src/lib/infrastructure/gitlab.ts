import type { Action, DataPackage, Project, ReadenFile } from "$lib/storage"

export const ciContent = `include:
  remote: https://gitlab.com/jailbreak/git-dms/minisite-generator/-/raw/main/ci-pipeline/build-minisite.yml`

export const fetchDataPackage = async (gitlabClient, projectPathWithNamespace: string): Promise<DataPackage | null> => {
	const { RepositoryFiles, Projects } = gitlabClient

	const project = await Projects.show(projectPathWithNamespace)

	const dataPackageRaw = await RepositoryFiles.showRaw(project.id, "datapackage.json", "HEAD")
	const datapackage = JSON.parse(dataPackageRaw)
	const projectUrl = project.web_url

	// Transform resource paths mentioned in datapackage.json to absolute URLs.
	const resources = datapackage.resources.map((resource) => {
		return {
			path: resource.path,
			url: `${projectUrl}/-/raw/${project.default_branch}/${resource.path}`,
		}
	})

	return {
		name: datapackage.name,
		description: datapackage.description,
		resources: resources,
		projectUrl: projectUrl,
		minisiteUrl: `https://${project.namespace.path}.gitlab.io/${project.path}`,
	}
}

export const fetchProject = async (gitlabClient, projectPathWithNamespace: string): Promise<Project | null> => {
	const { Projects } = gitlabClient

	const project = await Projects.show(projectPathWithNamespace)

	return getProject(project)
}

export const getProject = (project): Project => {
	return {
		id: project.id,
		name: project.path_with_namespace,
		defaultBranch: project.default_branch,
		url: project.web_url,
		ownerName: project.owner.username,
	}
}

export const buildActions = async (
	gitlabClient,
	project: Project,
	dataPackageJson: any,
	files: Array<ReadenFile>,
	filteredFiles: Array<string>,
	previousFiles: Array<string>,
	removedFiles: Array<string>
): Promise<Array<Action>> => {
	const { RepositoryFiles } = gitlabClient
	const resources = []

	// Add previous resources if file has not been marked for deletion
	for (const fileName of previousFiles) {
		if (!removedFiles.includes(fileName)) {
			resources.push(fileName)
		}
	}

	const actions = []

	// Remove and commit deletion of previous files marked for deletion
	if (removedFiles.length > 0) {
		for (const fileName of removedFiles) {
			actions.push({
				action: "delete",
				file_path: fileName,
			})
		}
	}

	let file: ReadenFile
	let action

	for (file of files) {
		const fileName: string = file.name
		if (filteredFiles.includes(fileName)) {
			if (previousFiles.includes(file.name)) {
				action = {
					action: "update",
					file_path: fileName,
					content: file.content,
				}
			} else {
				action = {
					action: "create",
					file_path: fileName,
					content: file.content,
				}
				resources.push(fileName)
			}
			actions.push(action)
		}
	}

	// sort files in alphabetical order
	resources.sort()

	for (const ressource of resources) {
		dataPackageJson.resources.push({
			path: ressource,
		})
	}

	actions.push({
		action: "update",
		file_path: "datapackage.json",
		content: JSON.stringify(dataPackageJson, null, 2),
	})

	const hasCI: boolean = await RepositoryFiles.show(project.id, ".gitlab-ci.yml", "HEAD").then(
		() => true,
		() => false
	)
	actions.push({
		action: hasCI ? "update" : "create",
		file_path: ".gitlab-ci.yml",
		content: ciContent,
	})

	return actions
}
