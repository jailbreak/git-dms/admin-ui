import { Gitlab } from "@gitbeaker/browser"
import BaseError from "baseerr"
import { filesToReadenFiles, readUploadedFileAsText } from "$lib/utils/file"
import type { User } from "$lib/model"
import type { StorageAdapter, Project, ProjectFilter, DataPackage, Action, ReadenFile } from "$lib/storage"
import { buildActions, ciContent, fetchDataPackage, fetchProject, getProject } from "$lib/infrastructure/gitlab"
import { CreateProjectError, CreateProjectDuplicateError } from "$lib/errors"

export default class GitLabStorageAdapter implements StorageAdapter {
	constructor(private gitlabClient, private user?: User) {}

	static create(config, tokens, userProfile) {
		const gitlabClient = new Gitlab({
			host: config.gitlabUrl,
			oauthToken: tokens.access_token,
		})
		return new GitLabStorageAdapter(gitlabClient, userProfile)
	}

	async createProject(name: string): Promise<Project> {
		const { Projects } = this.gitlabClient

		try {
			const project = await Projects.create({ name: name, visibility: "public" })

			return {
				id: project.id,
				name: project.path_with_namespace,
				defaultBranch: project.default_branch,
				url: project.http_url_to_repo,
				ownerName: project.owner.username,
			}
		} catch (error) {
			if (error?.description?.name[0] === "has already been taken") {
				throw CreateProjectDuplicateError.wrap(error, "A project with the same name already exists")
			} else {
				throw CreateProjectError.wrap(error, "create project error")
			}
		}
	}

	async createDataPackage(project: Project, formData: FormData, filteredFiles: Array<string>): Promise<boolean> {
		if (!this.user) {
			throw new Error("Unauthorized")
		}

		const { Commits } = this.gitlabClient

		const name: string = formData.get("datapackage-name") as string

		if (!name) {
			console.log("Error: Datapackage name required")
			return false
		}

		const description: string = formData.get("datapackage-description") as string | ""
		const formDataFiles: Array<File> = formData.getAll("datapackage-resource") as Array<File>
		const files = await filesToReadenFiles(formDataFiles)

		if (filteredFiles.length <= 0) {
			console.log("Error: Ressource files required")
			return false
		}

		// Datapackage specification :
		// https://specs.frictionlessdata.io/data-package
		// https://specs.frictionlessdata.io/data-resource/#introduction
		const dataPackageJson = {
			name: name,
			description: description,
			profile: "tabular-data-package",
			resources: [],
			contributors: [
				{
					title: this.user.username,
					role: "author",
				},
			],
		}

		let file: ReadenFile
		const actions = []
		const resources = []

		for (file of files) {
			const fileName: string = file.name
			if (filteredFiles.includes(fileName)) {
				actions.push({
					action: "create",
					file_path: fileName,
					content: file.content,
				})
				resources.push(file.name)
			}
		}

		// sort files in alphabetical order
		resources.sort()
		for (const ressource of resources) {
			dataPackageJson.resources.push({
				path: ressource,
			})
		}

		actions.push({
			action: "create",
			file_path: "datapackage.json",
			content: JSON.stringify(dataPackageJson, null, 2),
		})

		actions.push({
			action: "create",
			file_path: ".gitlab-ci.yml",
			content: ciContent,
		})

		await Commits.create(project.id, project.defaultBranch, "Create data package", actions)

		return true
	}

	async buildActions(
		project: Project,
		dataPackageJson: any,
		files: Array<ReadenFile>,
		filteredFiles: Array<string>,
		previousFiles: Array<string>,
		removedFiles: Array<string>
	): Promise<Array<Action>> {
		return buildActions(this.gitlabClient, project, dataPackageJson, files, filteredFiles, previousFiles, removedFiles)
	}

	async editDataPackage(
		project: Project,
		formData: FormData,
		filteredFiles: Array<string>,
		previousFiles: Array<string>,
		removedFiles: Array<string>
	): Promise<boolean> {
		if (!this.user) {
			throw new Error("Unauthorized")
		}

		const { Commits } = this.gitlabClient

		const name: string = formData.get("datapackage-name") as string

		if (!name) {
			console.log("Error: Datapackage name required")
			return false
		}

		const description: string = formData.get("datapackage-description") as string | ""

		const formDataFiles: Array<File> = formData.getAll("datapackage-resource") as Array<File>
		const files = await filesToReadenFiles(formDataFiles)

		if (filteredFiles.length <= 0) {
			console.log("Error: Ressource files required")
			return false
		}

		// Datapackage specification :
		// https://specs.frictionlessdata.io/data-package
		// https://specs.frictionlessdata.io/data-resource/#introduction
		const dataPackageJson = {
			name: name,
			description: description,
			profile: "tabular-data-package",
			resources: [],
			contributors: [
				{
					title: this.user.username,
					role: "author",
				},
			],
		}

		const actions = await this.buildActions(project, dataPackageJson, files, filteredFiles, previousFiles, removedFiles)

		// Commit updated datapackage.json and resources
		await Commits.create(project.id, project.defaultBranch, "Update data package", actions)

		return true
	}

	async contributeToDataPackage(
		project: Project,
		formData: FormData,
		filteredFiles: Array<string>,
		previousFiles: Array<string>,
		removedFiles: Array<string>
	): Promise<Record<string, unknown>> {
		if (!this.user) {
			throw new Error("Unauthorized")
		}

		const response = {
			data: {},
		}

		const { Branches, Commits, MergeRequests, Projects, RepositoryFiles } = this.gitlabClient

		const contributionComment: string = formData.get("contribution-comment") as string
		const formDataFiles: Array<File> = formData.getAll("datapackage-resource") as Array<File>
		const files = await filesToReadenFiles(formDataFiles)

		if (contributionComment === undefined || files.length === 0) {
			response.data["error"] = "Can not sent a contribution without a comment and resources update"
			return response
		}

		const dataPackageRaw = await RepositoryFiles.showRaw(project.id, "datapackage.json", "HEAD")
		const dataPackageJson = JSON.parse(dataPackageRaw)

		dataPackageJson.resources = []

		// Add the contributor in datapackage.json if is not already present
		const contributorEntry = {
			title: this.user.username,
			role: "contributor",
		}
		if (dataPackageJson.contributors) {
			const contributors = dataPackageJson.contributors.map((contributor) => contributor.title)
			if (!contributors.includes(this.user.username)) {
				dataPackageJson.contributors.push(contributorEntry)
			}
		} else {
			dataPackageJson.contributors = [contributorEntry]
		}

		let fork
		let contributorProject: Project | null = null

		if (project.ownerName !== this.user.username) {
			try {
				// try to create a fork
				fork = await Projects.fork(project.id)
			} catch {
				// if user has already forked the project, retrieve it
				const forks = await Projects.forks(project.id)
				// force pull
				fork = forks.find((f) => f.owner.username === this.user.username)
			}
			if (fork) {
				contributorProject = getProject(fork)
			}
		} else {
			// if user owns the project, retrieve it
			contributorProject = project
		}

		if (contributorProject === null) {
			response.data["error"] = "Can not find project to contribute to"
			return response
		}

		// create a branch with the timestamp and the project name
		const branchName = `${Date.now()}-${contributorProject.name}`
		try {
			await Branches.create(contributorProject.id, branchName, contributorProject.defaultBranch)
		} catch {
			response.data["error"] = `Can not create contribution branch ${branchName}`
			return response
		}

		console.log(
			`${branchName} created on ${contributorProject.name} : ${contributorProject.url}/-/commits/${branchName}`
		)

		const actions = await this.buildActions(project, dataPackageJson, files, filteredFiles, previousFiles, removedFiles)

		// Commit updated datapackage.json and resources
		await Commits.create(contributorProject.id, branchName, contributionComment, actions)

		// Create a merge request
		const mergeRequest = await MergeRequests.create(
			contributorProject.id,
			branchName,
			project.defaultBranch,
			contributionComment,
			{
				targetProjectId: project.id,
				remove_source_branch: true,
			}
		)

		const mergeRequestUrl = mergeRequest.web_url

		response.data["mergeRequestUrl"] = `${mergeRequestUrl}`
		return response
	}

	async fetchDataPackage(projectPathWithNamespace: string): Promise<DataPackage | null> {
		return fetchDataPackage(this.gitlabClient, projectPathWithNamespace)
	}

	async fetchProject(projectPathWithNamespace: string): Promise<Project | null> {
		return fetchProject(this.gitlabClient, projectPathWithNamespace)
	}

	async fetchProjects(filter: ProjectFilter = {}): Promise<Array<Project>> {
		const { RepositoryFiles, Users } = this.gitlabClient

		if (!this.user) {
			throw new Error("Unauthorized")
		}

		let userProjects = await Users.projects(this.user.username)
		userProjects = userProjects.map((project) => getProject(project))
		let filteredProjects = [...userProjects]

		// filter.onlyDataPackages XOR filter.onlyNoDataPackages
		if (
			(filter.onlyDataPackages && !filter.onlyNoDataPackages) ||
			(!filter.onlyDataPackages && filter.onlyNoDataPackages)
		) {
			filteredProjects = []
			for (const project of userProjects) {
				const hasDataPackage: boolean = await RepositoryFiles.show(project.id, "datapackage.json", "HEAD").then(
					() => true,
					() => false
				)
				if ((hasDataPackage && filter.onlyDataPackages) || (!hasDataPackage && filter.onlyNoDataPackages)) {
					filteredProjects.push(project)
				}
			}
		}

		return filteredProjects
	}
}
