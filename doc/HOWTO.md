# Create a new data package

Requires login

To create a new data package on https://jailbreak-dms.netlify.app :

1. Click on "new data package" on the home page or https://jailbreak-dms.netlify.app/data-packages

2. Fill out the form to create a data package:

   - **Name** \* : name of the data package

   - **Description** : description of the data package

   - **Resources** \* : list of data files of the datapackage

   - **Store Data Package on** \*: defines on which GitLab repository the data package will be stored
     - Note: If no repository is available (because all your repositories have already a `datapackage.json` or you don't have any repositories, you must first create a project on GitLab with "create a new project")

3. Once all the required fields (\*) are filled, submit the form with the button "Create Data Package"

Upon successful submission of the form, a data package will be created in the selected GitLab repository.

This datapackage contains :

- ressource files
- a special `datapackage.json` file with the data package metadata. ([See specification](https://specs.frictionlessdata.io//data-package/))
- a special `.gitlab-ci.yml` file used to automaticaly build a [minisite](#minisite) that allows other users to discover and [contribute](#contribute) to this data package

# Edit a data package

Requires login

You can only edit the data packages listed on your account, on this page : https://jailbreak-dms.netlify.app/data-packages

To edit a data package:

1. Click on the `View` button on the right of a data package name on https://jailbreak-dms.netlify.app/data-packages

2. Click on the `Edit Data Package` button on the `view` page of a data package

3. Fill out the form to update a data package, with the same fields as [create a new data package](#create-a-new-data-package) except `Store Data Package on` which is already defined

4. Once all the required fields (\*) are filled and at least one of them has changed, submit the form with the button "Update Data Package"

The files will be updated in the data package repository and the [minisite](#minisite) will be automaticaly rebuilt and updated

# minisite

Once a data package is created, a GitLab page is automatically availble.

This page displays information about the data package and includes a link to [contribute to it](#external-contributions-to-a-data-package).

# External contributions to a data package

When a user discovers a data package on a minisite, the user can access to a page to submit a proposal with changes by clicking `Contribute to this package`.

A contribution can come from:

- a [logged in user](#external-contributions---logged-in)

- an [anonymous user](#external-contributions---anonymous)

After the user submits a contribution, a GitLab merge request is created on the data package repository.

Once this merge request is accepted by the data package owner, the data package will be updated.

# External contributions - logged in

While logged in, an user can submit a data package contribution with these two fields :

- **Leave a comment to describe your proposal** \* : description of the contribution

- **Resources** \* : list of data files of the datapackage
  - To update a ressource, you must use the same name as the original files on your updated files.

# External contributions - anonymous

If a user is not logged in, the user can still submit a data package contribution.

The user has to fill the same fields as in [External contributions - logged in](#external-contributions---anonymous) and must provide an email
