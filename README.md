# Git DMS – Admin UI

Git DMS is a free and open source tool to manage open data.

This is an exploration for a new kind of data management system (DMS) powered by Git, which currently only supports [Gitlab.com](https://gitlab.com/).

[**Try our demo now!**](https://git-dms.netlify.app/)

Read the [HOWTO](doc/HOWTO.md) to learn all about the current features of Git DMS.

## Installation

```bash
git clone https://gitlab.com/jailbreak/git-dms/admin-ui.git
cd admin-ui
npm install
```

## Create application in GitLab

- Go to <https://gitlab.com/-/profile/applications>
- Create an application (e.g. named "DMS") with `http://localhost:3000/login/callback` as the redirect URL, non-confidental, and using the "api" and "openid" scopes
- Copy and save the credentials for later

## Configuration

Create a `.env` file and define `VITE_GITLAB_OAUTH_CLIENT_ID` from the previouly copied value.

## Running

```bash
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.
